extends Area2D
var espada=load("res://objetos/item_cofre.tscn")
var corazon=load("res://objetos/item_corazon.tscn")
var abierto=false


enum items{espada,corazon}

export (items) var item

func _ready():
	
	connect("area_entered",self,"area_adentro")
	
func area_adentro(area):
	if not abierto and area.is_in_group("toolPlayer"):
		abierto=true
		var balas=get_tree().get_nodes_in_group("bala")
		for i in balas:
			i.queue_free()
		
		var dir
		if Game.jugador.get_node("anim").flip_h:
			dir=Vector2.LEFT
		else:
			dir=Vector2.RIGHT
		$tapa.volar(dir)
			
		match item:
			items.espada:
				var item_i=espada.instance()
				add_child(item_i)
				Game.gui.set_arma("espada")
				Game.jugador.nueva_arma(load("res://armas/espada.tscn"))
			
			items.corazon:
				var item_i=corazon.instance()
				add_child(item_i)
				Game.gui.set_vida(4)
				Game.jugador.vida=4
