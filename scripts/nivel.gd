extends Node2D
var roca_r=load("res://efectos/roca_rota.tscn")
export (String) var bgm

func _ready():
	get_node("bgm/"+bgm).play()

func _process(delta):
	$gui/fps.text="fps: "+str(Engine.get_frames_per_second())
	if Input.is_action_just_pressed("click"):
		var roca=roca_r.instance()
		roca.global_position=get_global_mouse_position()
		add_child(roca)
	
	if Input.is_action_just_pressed("f"):
		OS.window_fullscreen=not OS.window_fullscreen
		

func stop_bgm():
	get_node("bgm/"+bgm).stop()
