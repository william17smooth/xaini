extends Node2D


var tiempo=0
var vida=10
var vida_brazo=5
var slimes=load("res://entidades/slimegirl.tscn")
var timer=Timer.new()
func _ready():
	$cabeza.connect("area_entered",self,"area_cabeza")
	$cabeza.connect("body_entered",self,"cuerpo_cabeza")
	$brazo.connect("area_entered",self,"area_brazo")
	$brazo.connect("body_entered",self,"cuerpo_brazo")
	set_physics_process(false)
	$anim.connect("animation_finished",self,"empezar")

func _physics_process(delta):
	if vida<=0 and vida_brazo<=0:
		fin()
		
	tiempo+=delta
	$cabeza.translate(Vector2((sin(tiempo)*60*delta),(sin(tiempo)*30*delta)))
	$brazo.translate(Vector2((sin(tiempo)*50*delta),(sin(tiempo*2)*10*delta)))
	
func area_cabeza(area):
	if area.is_in_group("punchPlayer") or area.is_in_group("swordPlayer"):
		vida-=1
		$anim.play("hit")
		if vida<=0:
			$cabeza/col.queue_free()
			$cabeza/spr.hide()

func area_brazo(area):
	if area.is_in_group("punchPlayer") or area.is_in_group("swordPlayer"):
		vida_brazo-=1
		$anim.play("hit")
		if vida_brazo<=0:
			$brazo/col.queue_free()
			$brazo/spr.hide()
			

func cuerpo_brazo(cuerpo):
	if cuerpo.is_in_group("jugador"):
		cuerpo.auch()

func cuerpo_cabeza(cuerpo):
	if cuerpo.is_in_group("jugador"):
		cuerpo.auch()

func empezar(anim):
	set_physics_process(true)

#func disparar():
#	for i in range(5):
#		var bala= bala_r.instance()
#		bala.global_position=$spawn_balas.global_position
#		bala.dir=Vector2.UP.rotated(PI/(5-1)*i)
#		bala.dir.x*=direccion.x
#		owner.add_child(bala)

func fin():
	var balas=get_tree().get_nodes_in_group("bala")
	var enemigos=get_tree().get_nodes_in_group("enemigo")
	for i in balas:
		i.queue_free()
	for i in enemigos:
		i.queue_free()
	
	set_physics_process(false)
	add_child(timer)
	timer.start(5)
	yield(timer,"timeout")
	Game.gui.ganaste()
