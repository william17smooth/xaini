extends KinematicBody2D

var estado="idle"
var velocidad=Vector2.ZERO
var acel=5
var max_vel=75
var gravedad=9
var salto=185
var carga=0
var vida=4
var timer=Timer.new()
var timer_carga=Timer.new()
var pausa=false
export (PackedScene) var arma

func _ready():
	Game.jugador=self
	var enemigos=get_tree().get_nodes_in_group("enemigo")
	for e in enemigos:
		add_collision_exception_with(e)
	
	if Game.arma=="punch":
		arma=load("res://armas/arma.tscn").instance()
	else:
		arma=load("res://armas/espada.tscn").instance()
	
	Game.gui.set_arma(Game.arma)
	add_child(arma)
	arma.connect("body_entered",self,"ataque")
	add_child(timer)
	add_child(timer_carga)

func _physics_process(delta):
	Game.gui.get_node("estado").text=estado
	velocidad.y+=gravedad
	
	if global_position.y>160 or vida<=0:
		hide()
		Game.gui.get_node("cinematica/anim").play("transicion")
		set_physics_process(false)
		get_parent().stop_bgm()
		yield(Game.gui.get_node("cinematica/anim"),"animation_finished")
		get_tree().reload_current_scene()
	
	if not pausa:
		match estado:
			"idle":
				if velocidad.x!=0:
					estado="stoping"
				elif Input.is_action_pressed("abajo") and Game.gui.get_node("barra_poder").visible:
					estado="especial"
				if Input.is_action_pressed("derecha"):
					
					estado="walk"
					flip(false)
					
				elif Input.is_action_pressed("izquierda"):
					
					estado="walk"
					flip(true)
				
	
			"walk":
				if not is_on_floor():
					estado="jump"
				if Input.is_action_pressed("derecha"):
					velocidad.x=min(max_vel,velocidad.x+acel)
				if Input.is_action_pressed("izquierda"):
					velocidad.x=max(-max_vel,velocidad.x-acel)
				
				if Input.is_action_just_released("derecha"):
					estado="idle"
				if Input.is_action_just_released("izquierda"):
					
					estado="idle"
	
			"stoping":
				if velocidad.x>0:
					velocidad.x=max(velocidad.x-acel*0.7,0)
				elif velocidad.x<0:
					velocidad.x=min(velocidad.x+acel*0.7,0)
				else:
					estado="idle"
			
				if Input.is_action_pressed("derecha"):
					
					flip(false)
					
				if Input.is_action_pressed("izquierda"):
	
					flip(true)
				
			"especial":
				carga+=2
				carga=clamp(carga,0,100)
				Game.gui.set_poder(carga)
				
				if Input.is_action_just_released("abajo") or not is_on_floor():
					estado="idle"
					timer_carga.start(0.2)
					yield(timer_carga,"timeout")
					carga=0
					Game.gui.set_poder(carga)

				
			"jump":
				
				
				if Input.is_action_pressed("derecha"):
					velocidad.x=min(max_vel,velocidad.x+acel)
				if Input.is_action_pressed("izquierda"):
					velocidad.x=max(-max_vel,velocidad.x-acel)
				
				if is_on_floor():
					estado="idle"
		if Input.is_action_pressed("b"):
			atacar()
		if is_on_floor() and estado!="especial":
			if Input.is_action_just_pressed("a"):
				velocidad.y=-salto
				estado="jump"
	
	$anim.play(estado)
	velocidad=move_and_slide(velocidad,Vector2.UP)

func ataque(cuerpo):
	if cuerpo.is_in_group("roca"):
		cuerpo.romper()
		cuerpo.queue_free()
#	elif cuerpo.is_in_group("mapa"):
#		velocidad=Vector2(-75,-200)
		

func atacar():
	if not arma.get_node("anim").is_playing():
#		if $anim.flip_h:
#			arma.get_node("spr").position.x=-12
#			arma.get_node("CollisionShape2D").position.x=-arma.posicion.x
#		else:
#			arma.get_node("spr").position.x=12
#			arma.get_node("CollisionShape2D").position.x=arma.posicion.x
	
		
		arma.get_node("anim").play("atacar")
		get_tree().get_nodes_in_group("sfx")[0].get_node("punch").play()

func flip(vflip):
	$anim.flip_h=vflip
	if vflip:
		arma.get_node("spr").position.x=-12
		arma.get_node("CollisionShape2D").position.x=-arma.posicion.x
	else:
		arma.get_node("spr").position.x=12
		arma.get_node("CollisionShape2D").position.x=arma.posicion.x
	arma.get_node("spr").flip_h=vflip

func auch():
	remove_from_group("jugador")
	if $anim.flip_h:
		velocidad=Vector2(100,-100)
	else:
		velocidad=Vector2(-100,-100)
	vida-=1
	Game.gui.set_vida(vida)
	timer.start(1.5)
	$anim_spr.play("golpeado")
	yield(timer,"timeout")
	$anim_spr.stop()
	modulate=Color(1.0,1.0,1.0,1.0)
	add_to_group("jugador")

func pausar():
	pausa=true
	velocidad.x=0
	estado="idle"

func nueva_arma(arma_nueva):
	arma.queue_free()
	arma=arma_nueva.instance()
	call_deferred("add_child",arma)
	arma.connect("body_entered",self,"ataque")
