extends Area2D

export (String) var siguiente_nivel

func _ready():
	connect("body_entered",self,"siguiente")


func siguiente(cuerpo):
	if cuerpo.is_in_group("jugador"):
		Game.gui.get_node("cinematica/anim").play("transicion")
		yield(Game.gui.get_node("cinematica/anim"),"animation_finished")
		get_tree().change_scene(siguiente_nivel)
