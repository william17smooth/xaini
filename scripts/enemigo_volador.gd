extends Area2D

var velocidad=Vector2.ZERO
var vivo=true
var cont=0
func _ready():
	$vis.connect("screen_exited",self,"desaparecer")
	$vis.connect("screen_entered",self,"aparecer")
	connect("body_entered",self,"ataque_mele")
	connect("area_entered",self,"ataque_area")
	set_physics_process(false)
	
func _physics_process(delta):
	
	if not Game.jugador.pausa:
		if vivo:
			cont+=delta*3
			velocidad=Vector2(-100,(sin(cont)*45))
		#	if velocidad.y<0:
		#		$spr.play("default")
		#	else:
		#		if $spr.frame==0:
		#			$spr.stop()
		else:
			
			velocidad.y+=5
		
		translate(velocidad*delta)

func desaparecer():
	
	queue_free()

func aparecer():
	set_physics_process(true)

func ataque_mele(cuerpo):
	if cuerpo.is_in_group("jugador"):
		cuerpo.auch()

func ataque_area(area):
	if area.is_in_group("punchPlayer") or area.is_in_group("swordPlayer"):
		morir()

func morir():
	$spr.stop()
	$spr.flip_v=true
	$spr.rotation_degrees=90
	velocidad.y=-200
	vivo=false
	$col.queue_free()
	
	
