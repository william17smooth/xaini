extends Sprite

var velocidad=Vector2.ZERO

func _ready():
	set_physics_process(false)
	

func _physics_process(delta):
	velocidad.y+=5
	
	translate(velocidad*delta)
	if global_position.y>160:
		queue_free()
	
func volar(dir):
	set_physics_process(true)
	velocidad.x=dir.x*25
	velocidad.y=-100
