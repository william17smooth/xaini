extends Area2D

var dir=Vector2.ZERO

func _ready():
	connect("body_entered",self,"colisiono")
	connect("area_entered",self,"colisiono_area")
	$vis.connect("screen_exited",self,"me_voy")

func _physics_process(delta):
	translate(dir*100*delta)

func colisiono(cuerpo):
	if cuerpo.is_in_group("jugador"):
		cuerpo.auch()
		queue_free()

func colisiono_area(area):
	if area.is_in_group("punchPlayer"):
		queue_free()
	elif area.is_in_group("swordPlayer"):
		dir.x*=-1
		dir.y=-1

func me_voy():
	queue_free()
