extends Node2D

var velocidad=Vector2()
func _ready():
	$aparecer.connect("animation_finished",self,"fin")
	set_physics_process(false)
	get_tree().get_nodes_in_group("gui")[0].modo_cinematica(true)
	
func _physics_process(delta):
	velocidad.y+=3
	$spr.translate(velocidad*delta)
	if $spr.global_position.y>160:
		$spr.queue_free()
		set_physics_process(false)
		
		get_tree().get_nodes_in_group("gui")[0].modo_cinematica(false)
	
func fin(anim):
	$anim.stop()
	$spr.frame=0
	z_index=1
	var timer=Timer.new()
	add_child(timer)
	timer.start(1)
	yield(timer,"timeout")
	set_physics_process(true)
