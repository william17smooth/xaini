extends Area2D

var dir=Vector2()

func _ready():
	connect("body_entered",self,"pared")
	$vis.connect("screen_exited",self,"desaparecer")

func _physics_process(delta):
	translate(dir*200*delta)

func pared(body):
	if not body.is_in_group("jugador"):
		queue_free()

func desaparecer():
	queue_free()
