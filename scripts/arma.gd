extends Area2D
export (Vector2) var posicion
var efecto_r=load("res://objetos/efecto_espada.tscn")

func _ready():
	pass

func atacar():
	if Game.jugador.carga>=90:
		var efecto=efecto_r.instance()
		if Game.jugador.get_node("anim").flip_h:
			efecto.dir=Vector2.LEFT
			efecto.get_node("spr").flip_h=true
		else:
			efecto.dir=Vector2.RIGHT
		efecto.global_position=global_position+Vector2(0,16)*efecto.dir
		get_tree().current_scene.add_child(efecto)
		Game.jugador.carga=0

