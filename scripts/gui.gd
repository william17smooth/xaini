extends CanvasLayer

var timer=Timer.new()
func _ready():
	Game.gui=self
	set_poder(0)
	$cinematica.show()

func modo_cinematica(activado):
	if activado:
		$cinematica/anim.play("activar")
		Game.jugador.carga=0
		set_poder(0)
		Game.jugador.pausar()
	else:
		$cinematica/anim.play("desactivar")
		yield($cinematica/anim,"animation_finished")
		get_tree().get_nodes_in_group("jugador")[0].pausa=false

func set_poder(value):
	value=range_lerp(value,0,100,0,32)
	value=int(value)
	$barra_poder/barra.region_rect=Rect2(48,80,value,16)

func set_arma(arma):
	match arma:
		"espada":
			get_node("arma/arma").frame=1
			get_node("barra_poder").show()
		"punch":
			get_node("arma/arma").frame=0
	Game.arma=arma

func ganaste():
	$cinematica/anim.play("ganaste")
	yield($cinematica/anim,"animation_finished")
	add_child(timer)
	timer.start(5)
	yield(timer,"timeout")
	get_tree().change_scene("res://escenas/pantalla_principal.tscn")
	

func set_vida(value):
	match value:
		1:
			$"vida/00".hide()
			$"vida/01".hide()
			$"vida/02".hide()
		2:
			$"vida/00".show()
			$"vida/01".hide()
			$"vida/02".hide()
		3:
			$"vida/00".show()
			$"vida/01".show()
			$"vida/02".hide()
		4:
			$"vida/00".show()
			$"vida/01".show()
			$"vida/02".show()
