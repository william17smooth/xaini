extends Control
var vel=15
var offset_x=0
func _ready():
	pass

func _process(delta):
	offset_x-=vel*delta
	$paralax.scroll_offset=Vector2(offset_x,0)
	if Input.is_action_just_pressed("ui_accept"):
		$press_start.hide()
		$titulo.hide()
		$anim.play("start")
		yield($anim,"animation_finished")
		$anim.play("transicion")
		yield($anim,"animation_finished")
		get_tree().change_scene("res://escenas/nivel_1.tscn")
