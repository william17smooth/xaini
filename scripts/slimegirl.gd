extends KinematicBody2D

var bala_r=load("res://objetos/bala.tscn")

export (int) var cant_disparos=5
var velocidad=Vector2(-40,0)
var vel=40
var gravedad=9
export (bool) var puedo_disparar=true


func _ready():
	add_collision_exception_with(Game.jugador)
	$vis.connect("screen_entered",self,"soy_visible")
	$vis.connect("screen_exited",self,"no_soy_visible")
	$timer.connect("timeout",self,"ya_puedo_disparar")
	$ataque.connect("body_entered",self,"ataque_mele")
	$ataque.connect("area_entered",self,"ataque_area")
	
	set_physics_process(false)

func _physics_process(delta):
	velocidad.y+=gravedad
	
	if abs(global_position.x-Game.jugador.global_position.x)<100:
		if global_position.x<Game.jugador.global_position.x and not $spr.flip_h:
			atacar(Vector2.RIGHT)
		elif global_position.x>Game.jugador.global_position.x and $spr.flip_h:
			atacar(Vector2.LEFT)
	
	if is_on_wall() or not $derecha.is_colliding() or not $izquierda.is_colliding():
		velocidad.x*=-1
		$spr.flip_h=not $spr.flip_h
		$spawn_balas.position.x*=-1
	
	if is_on_floor():
		velocidad.y=0
		$spr.animation="default"

	
	
	move_and_slide(velocidad,Vector2.UP)

func atacar(direccion):
	if puedo_disparar and not Game.jugador.pausa:
		$timer.start()
		puedo_disparar=false
		
		for i in range(cant_disparos):
			var bala= bala_r.instance()
			bala.global_position=$spawn_balas.global_position
			bala.dir=Vector2.UP.rotated(PI/(cant_disparos-1)*i)
			bala.dir.x*=direccion.x
			owner.add_child(bala)

func ataque_mele(cuerpo):
	if cuerpo.is_in_group("jugador"):
		cuerpo.auch()

func ataque_area(area):
	if area.is_in_group("punchPlayer") or area.is_in_group("swordPlayer"):
		morir()

func ya_puedo_disparar():
	puedo_disparar=true

func morir():
	var dead=load("res://objetos/slime_dead.tscn").instance()
	dead.global_position=global_position

	
	owner.add_child(dead)

	queue_free()

func soy_visible():
	set_physics_process(true)

func no_soy_visible():
	set_physics_process(false)
	
